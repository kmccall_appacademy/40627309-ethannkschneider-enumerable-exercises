require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0, :+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |long_string| substring?(long_string, substring) }
end

def substring?(big_string, small_string)
  big_string.include?(small_string)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letters = string.split.join.split("")
  letters_index = letters

  dup_letters = letters_index.select { |ch| letters.count(ch) > 1 }
  dup_letters.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split
  words.sort! { |word1, word2| word1.length <=> word2.length }
  [words[-1], words[-2]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = "abcdefghijklmnopqrstuvwxyz"
  result = []
  alphabet.chars.each do |ch|
    result << ch unless string.include?(ch)
  end

  result
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| not_repeat_year?(year)}
end

def not_repeat_year?(year)
  year.to_s.split("") == year.to_s.split("").uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select! { |song| no_repeats?(song, songs) }
  songs.uniq
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, index|
    return false if song == song_name && (songs[index - 1] == song || songs[index + 1] == song)
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = remove_punctuation(string.downcase).split
  words.select! { |word| word.include?("c") }
  return "" if words.empty?
  sorted_words = words.sort { |word| c_distance(word) }
  lowest_dist = c_distance(sorted_words[0])
  words.select! { |word| c_distance(word) == lowest_dist }
  words[0]
end

def c_distance(word)
  reverse_word = word.reverse
  reverse_word.index("c")
end

def remove_punctuation(str)
  punctuation = ".,/;:?!"
  str.delete(punctuation)
end


# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  sub_chunk = []

  arr.each_with_index do |num, index|
    if sub_chunk.empty?
      sub_chunk << index
    elsif arr[sub_chunk.last] == num
      sub_chunk << index
      result << sub_chunk if index == (arr.length - 1)
    else
      result << sub_chunk if sub_chunk.count > 1
      sub_chunk = []
      sub_chunk << index
    end
  end

  result.map { |sub_arr| [sub_arr.first, sub_arr.last] }
end
